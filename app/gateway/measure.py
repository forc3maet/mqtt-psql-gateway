from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, String, DateTime, ForeignKey

Base = declarative_base()


class Measure(Base):
    """Hold sensor data about current state"""
    __tablename__ = 'measures'

    id = Column(Integer, primary_key=True)
    # sensor_id = Column(String, ForeignKey("sensor.id"))
    sensor = Column(String)
    measure = Column(String)
    value = Column(Float)
    timestamp = Column(DateTime, default=datetime.utcnow)

    def __init__(self, sensor, measure, value):
        self.sensor = sensor
        self.measure = measure
        self.value = value

    def __repr__(self):
        return f"Measure(sensor={self.sensor!r}, measure={self.measure!r}, value={self.value!r})"
