from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


Base = declarative_base()


class Sensor(Base):
    """Define list of the available sensors"""
    __tablename__ = 'sensors'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    # name = relationship(Measure)
    alias = Column(String)


    def __repr__(self):
        return f"Sensor(id={self.id!r}, name={self.name!r}, alias={self.alias!r})"
