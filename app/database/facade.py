from datetime import datetime
from sqlalchemy import MetaData
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, Float, String, DateTime, ForeignKey
import config


metadata = MetaData()
metadata.bind = create_engine(config.SQL_URL)

sensor_list = Table(
    "sensors",
    metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String, unique=True),
    Column('alias', String)
)

sensor_data = Table(
    "measures",
    metadata,
    Column('id', Integer, primary_key=True),
    Column('sensor', String),
    Column('measure', String),
    Column('value', Float),
    # Column('sensor_id', ForeignKey('sensors.id')),
    Column('timestamp', DateTime, default=datetime.utcnow)
)

# TIP: should executed once to create all tables
metadata.create_all()
