# TIP: just save manually executed queries for easy access
insert_sensor = '''
INSERT INTO sensors (name, alias) 
VALUES('lywsd03mmc', 'box');
'''

insert_measure = '''
INSERT INTO measures (sensor, measure, value, timestamp)
VALUES ({temperature}, {moisture}, {light}, {conductivity}, {battery}, '{date}'::timestamp);
'''
