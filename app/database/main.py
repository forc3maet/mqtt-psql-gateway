from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from json_logging import get_logger
import config

logger = get_logger(__name__)


class DatabaseClient:
    def __init__(self):
        try:
            self.engine = create_engine(config.SQL_URL)  # TODO: add auto retry in case of connection failure
            self.session_factory = sessionmaker(bind=self.engine, autocommit=False)
        except Exception as err:  # TODO: catch sql specific exceptions
            logger.error("Failed to setup DB connection", extra='{"error": "%s"}' % str(err))
            exit(1)

    def get_session(self):
        return self.session_factory()
