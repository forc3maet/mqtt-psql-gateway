import os
from dotenv import load_dotenv


load_dotenv()


LOG_LEVEL = os.getenv("LOG_LEVEL", "DEBUG")

DATABASE = {
    "host": os.getenv("DATABASE_HOST"),
    "user": os.getenv("DATABASE_USER"),
    "database": os.getenv("DATABASE_NAME"),
    "password": os.getenv("DATABASE_PASS"),
    "port": 5432
}

SQL_URL = "postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}".format(**DATABASE)

MQTT = {
    "host": os.getenv("MQTT_HOST"),
    "base_topic": os.getenv("MQTT_BASE_TOPIC"),
    "client_id": "python_gateway"
}

DEVICES = [
    {
        "name": "xiaomi_lywsd03mmc",
        "mac": "A4:C1:38:89:DB:23",
        "alias": "box"
    }
]