from pythonjsonlogger import jsonlogger
import logging
import config


def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(config.LOG_LEVEL)
    json_handler = logging.StreamHandler()
    formatter = jsonlogger.JsonFormatter(
        fmt='%(asctime)s %(levelname)s %(name)s %(message)s'
    )
    json_handler.setFormatter(formatter)
    logger.addHandler(json_handler)
    return logger
