import paho.mqtt.client as mqtt
from gateway.measure import Measure
from database.main import DatabaseClient

from json_logging import get_logger

logger = get_logger(__name__)
session = DatabaseClient().get_session()


class MosquittoClient:

    def __init__(self, kwargs):
        self.host = kwargs.get('host', "")
        self.topic = kwargs.get('base_topic', "")

    def subcribe(self, client, userdata, flags, rc):
        """
        Subscribe to the esphome base topic
        """
        client.subscribe(self.topic)
        logger.info("Connected to MQTT server with result code " + str(rc))


    def enrich_psql(self, client, userdata, message):
        """
        Get info of the gateway from esphome and pass to psql
        example of the message: base_topic/sensor/name_measurement/state value
        """

        value = float(message.payload.decode("utf-8"))
        topic = message.topic
        sensor = topic.split('/')[2].split('_')[0]
        measure = topic.split('/')[2].split('_')[-1]
        measurement = Measure(sensor, measure, value)
        session.add(measurement)
        session.commit()
        logger.info("Received record for {data} from esp for sensor {name}.".format(data=measure, name=sensor))

    def start(self):
        client = mqtt.Client()  # TODO: add auto retry in case of connection failure
        client.on_connect = self.subcribe
        client.on_message = self.enrich_psql

        try:
            client.connect(self.host)
        except Exception as err:  # TODO: catch mqtt specific exceptions
            logger.error("Failed to setup MQTT connection", extra='{"error": "%s"}' % str(err))
            exit(1)
        client.loop_forever()


if __name__ == "__main__":
    MosquittoClient().start()
