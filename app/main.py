from mosquitto.main import MosquittoClient
import config

if __name__ == "__main__":
    MosquittoClient(config.MQTT).start()

