#!/usr/bin/env bash
set -euo pipefail

echo "[ $(date +"%Y-%m-%d %H:%M:%S.%N %:z") ] Generate .env file."
printenv | grep APP_CONF__ | awk -F 'APP_CONF__'  '{print $2}' > app/.env
